# SiLA Base
This repository contains base definitions of the SiLA standard, such as the feature schemas and framework 
protos. Additionally it contains the SiLA Features of the standardization group.

More information can be found on our [documentation](https://sila2.gitlab.io/sila_base/).

## Style Guidelines
The tabbing in the xml documents is done with 2 spaces.

# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).
